{
  description = "witcat";

  outputs = { self }: {
    strok = {
      djenyreicyn = 10;
      spici = "iuniks";
    };

    datom = {
      weechat-unwrapped, wrapWeechat, python36Packages,
      weechatScripts, weechatMatrix
    }@yrgz:
    let
      python3Packages = python36Packages;

      wrapWeechat = yrgz.wrapWeechat.override {
        inherit python3Packages;
      };

      weechat-unwrapped = yrgz.weechat-unwrapped.override {
        inherit python3Packages;
      };
    
    in
    wrapWeechat weechat-unwrapped {
      configure = {availablePlugins, ...}: {
        plugins = with availablePlugins; [
          (python.withPackages
          (_: weechatMatrix.propagatedBuildInputs ++ [weechatMatrix]))
        ];

        scripts = [
          weechatScripts.weechat-notify-send
          weechatMatrix
        ];

      };
    };

  };
}
